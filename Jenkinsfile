pipeline {

    options {
        buildDiscarder(logRotator(numToKeepStr:'3', artifactNumToKeepStr:'3'))
    }

    agent any

    parameters {
        string(name: 'tests', defaultValue: 'RunCukes', description: 'All completed cucumber tests')
        string(name: 'url', defaultValue: 'http://toolslist.safebear.co.uk:8080', description: 'test environment')
        string(name: 'browser', defaultValue: 'headless', description: 'chrome headless browser')
        string(name: 'sleep', defaultValue: '0', description: 'zero out any sleep commands')
    }

    triggers { pollSCM('* * * * *') } // poll the source code repo every minute.

    stages {

        stage('Smoke tests') {
            steps {
                bat "mvn -Dtest=RunCukesSmoke test -Durl=${params.url} -Dbrowser=${params.browser} -Dsleep=${params.sleep}"
            }

        }

        stage('BDD Requirements Testing') {
            steps {
                bat "mvn -Dtest=${params.bddtests} test -Durl=${params.url} -Dbrowser=${params.browser} -Dsleep=${params.sleep}"
            }
            post {
                always {
                    publishHTML([
                            allowMissing         : false,
                            alwaysLinkToLastBuild: false,
                            keepAll              : false,
                            reportDir            : 'target/cucumber-reports',
                            reportFiles          : 'reports.html',
                            reportName           : 'BDD Report',
                            reportTitles         : ''])
                }
            }
        }

    }

}