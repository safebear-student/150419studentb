Feature: Login
  In order to
  As a
  I want to

  Rules:
  - User must be informed if login info is invalid
  - User must be informed if login is successful
  - There must be an option to reset the user password

  Glossary:
  - CRUD = Create, read, update, delete

  Questions:
  - Do users get locked out with too many attempts

  @HighRisk @HighImpact
  @Regression
  @smoke
  Scenario Outline: A user logs into the application
    Given I nav to the login page
    When I enter login details for '<userType>'
    Then I can see the following message '<validationMessage>'
    Examples:
      | userType    | validationMessage                 |
      | invalidUser | Username or Password is incorrect |
      | validUser   | Login Successful                  |