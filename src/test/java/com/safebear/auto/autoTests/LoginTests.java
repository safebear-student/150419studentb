package com.safebear.auto.autoTests;

import com.safebear.auto.utils.Utils;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LoginTests extends BaseTest {

    @Test
    public void loginAsValidUserAndCheckMessageTest() {
        driver.get(Utils.getUrl());
        Assert.assertEquals(loginPage.getPageTitle(), loginPage.getExpectedPageTitle());
        loginPage.enterUsername("tester");
        loginPage.enterPassword("letmein");
        loginPage.clickSubmitButton();
        Assert.assertEquals(toolsPage.getPageTitle(), toolsPage.getExpectedPageTitle());

    }

    @Test
    public void loginAsInvalidUserTest(){
        driver.get(Utils.getUrl());
        Assert.assertEquals(loginPage.getPageTitle(), loginPage.getExpectedPageTitle());
        loginPage.enterUsername("hack");
        loginPage.enterPassword("letmein");
        loginPage.clickSubmitButton();
        Assert.assertEquals(loginPage.getPageTitle(), loginPage.getExpectedPageTitle());

    }

}
