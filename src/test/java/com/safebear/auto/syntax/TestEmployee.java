package com.safebear.auto.syntax;

import org.testng.annotations.Test;

public class TestEmployee {

    @Test
    public void testEmployee(){
        // create an employee called bob
        Employee bob = new Employee();
        SalesEmployee tina = new SalesEmployee();
        Employee richie = new Employee();

        bob.employ();
        tina.employ();
        richie.employ();
        bob.givePayRise(1000);
        tina.changeCar("Tesla");

        System.out.println("Bob's employment status: " + bob.employed);
        System.out.println("Bob's Salary: " + bob.salary);
        System.out.println("Number of employees " + Employee.numEmployees);

        bob.fire();

        System.out.println("Number of employees " + Employee.numEmployees);




    }
}
