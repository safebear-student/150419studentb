package com.safebear.auto.pages.locators;

import lombok.Data;
import lombok.Getter;
import org.openqa.selenium.By;


@Getter
public class LoginPageLocators {

    private By usernameFieldLocator = By.id("username");
    private By passwordFieldLocator = By.name("psw");
    private By submitButtonLocator = By.id("enter");

    private By validationWarningMessageLocator = By.xpath(".//p[@id='rejectLogin']/b");
}
