package com.safebear.auto.pages.locators;

import lombok.Data;
import org.openqa.selenium.By;

@Data
public class ToolsPageLocators {

    private By loginSuccessfulMessageLocator = By.xpath("//div[@class='container']/p/b/.");
}
