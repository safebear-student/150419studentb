package com.safebear.auto.syntax;

public class Employee {
    boolean employed;
    int salary;
    static int numEmployees;

    public void fire(){
        employed = false;
        numEmployees = numEmployees -1;
    //    numEmployees--;
    }

    public void employ(){
        employed = true;
        numEmployees++;
    }

    public void givePayRise(int payRiseAmount){
        salary = salary + payRiseAmount;
    }
}
