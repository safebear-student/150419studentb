$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("toolslist.features/login.feature");
formatter.feature({
  "line": 1,
  "name": "Login",
  "description": "In order to\r\nAs a\r\nI want to\r\n\r\nRules:\r\n- User must be informed if login info is invalid\r\n- User must be informed if login is successful\r\n- There must be an option to reset the user password\r\n\r\nGlossary:\r\n- CRUD \u003d Create, read, update, delete\r\n\r\nQuestions:\r\n- Do users get locked out with too many attempts",
  "id": "login",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "line": 20,
  "name": "A user logs into the application",
  "description": "",
  "id": "login;a-user-logs-into-the-application",
  "type": "scenario_outline",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 17,
      "name": "@HighRisk"
    },
    {
      "line": 17,
      "name": "@HighImpact"
    },
    {
      "line": 18,
      "name": "@Regression"
    },
    {
      "line": 19,
      "name": "@Smoke"
    }
  ]
});
formatter.step({
  "line": 21,
  "name": "I nav to the login page",
  "keyword": "Given "
});
formatter.step({
  "line": 22,
  "name": "I enter login details for \u0027\u003cuserType\u003e\u0027",
  "keyword": "When "
});
formatter.step({
  "line": 23,
  "name": "I can see the following message \u0027\u003cvalidationMessage\u003e\u0027",
  "keyword": "Then "
});
formatter.examples({
  "line": 24,
  "name": "",
  "description": "",
  "id": "login;a-user-logs-into-the-application;",
  "rows": [
    {
      "cells": [
        "userType",
        "validationMessage"
      ],
      "line": 25,
      "id": "login;a-user-logs-into-the-application;;1"
    },
    {
      "cells": [
        "invalidUser",
        "Username or Password is incorrect"
      ],
      "line": 26,
      "id": "login;a-user-logs-into-the-application;;2"
    },
    {
      "cells": [
        "validUser",
        "Login Successful"
      ],
      "line": 27,
      "id": "login;a-user-logs-into-the-application;;3"
    }
  ],
  "keyword": "Examples"
});
formatter.before({
  "duration": 6883678700,
  "status": "passed"
});
formatter.scenario({
  "line": 26,
  "name": "A user logs into the application",
  "description": "",
  "id": "login;a-user-logs-into-the-application;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 17,
      "name": "@HighRisk"
    },
    {
      "line": 19,
      "name": "@Smoke"
    },
    {
      "line": 17,
      "name": "@HighImpact"
    },
    {
      "line": 18,
      "name": "@Regression"
    }
  ]
});
formatter.step({
  "line": 21,
  "name": "I nav to the login page",
  "keyword": "Given "
});
formatter.step({
  "line": 22,
  "name": "I enter login details for \u0027invalidUser\u0027",
  "matchedColumns": [
    0
  ],
  "keyword": "When "
});
formatter.step({
  "line": 23,
  "name": "I can see the following message \u0027Username or Password is incorrect\u0027",
  "matchedColumns": [
    1
  ],
  "keyword": "Then "
});
formatter.match({
  "location": "StepDefs.i_nav_to_the_login_page()"
});
formatter.result({
  "duration": 2334579100,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "invalidUser",
      "offset": 27
    }
  ],
  "location": "StepDefs.i_enter_login_details_for_a_User(String)"
});
formatter.result({
  "duration": 396130600,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Username or Password is incorrect",
      "offset": 33
    }
  ],
  "location": "StepDefs.i_can_see_the_following_message(String)"
});
formatter.result({
  "duration": 48837000,
  "status": "passed"
});
formatter.after({
  "duration": 3206650900,
  "status": "passed"
});
formatter.before({
  "duration": 3948274100,
  "status": "passed"
});
formatter.scenario({
  "line": 27,
  "name": "A user logs into the application",
  "description": "",
  "id": "login;a-user-logs-into-the-application;;3",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 17,
      "name": "@HighRisk"
    },
    {
      "line": 19,
      "name": "@Smoke"
    },
    {
      "line": 17,
      "name": "@HighImpact"
    },
    {
      "line": 18,
      "name": "@Regression"
    }
  ]
});
formatter.step({
  "line": 21,
  "name": "I nav to the login page",
  "keyword": "Given "
});
formatter.step({
  "line": 22,
  "name": "I enter login details for \u0027validUser\u0027",
  "matchedColumns": [
    0
  ],
  "keyword": "When "
});
formatter.step({
  "line": 23,
  "name": "I can see the following message \u0027Login Successful\u0027",
  "matchedColumns": [
    1
  ],
  "keyword": "Then "
});
formatter.match({
  "location": "StepDefs.i_nav_to_the_login_page()"
});
formatter.result({
  "duration": 1994708800,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "validUser",
      "offset": 27
    }
  ],
  "location": "StepDefs.i_enter_login_details_for_a_User(String)"
});
formatter.result({
  "duration": 1767128900,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Login Successful",
      "offset": 33
    }
  ],
  "location": "StepDefs.i_can_see_the_following_message(String)"
});
formatter.result({
  "duration": 51050800,
  "status": "passed"
});
formatter.after({
  "duration": 3104668800,
  "status": "passed"
});
});